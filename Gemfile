source 'https://rubygems.org'
ruby '2.3.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.7.1'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 5.0.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Bootstrap
gem 'bootstrap-sass'

# Font Awesome
gem 'font-awesome-sass'

# Frontend
gem 'lodash-rails'
gem 'selectize-rails'
gem 'vuejs-rails'

# Web Server
gem 'puma'

# Worker Server
gem 'sidekiq'
# For checking progress of a background job
gem 'sidekiq-status'
# For managing retries of a background job
gem 'sidekiq-retries'
# For viewing failed jobs
gem 'sidekiq-failures', git: 'https://github.com/mhfs/sidekiq-failures.git'

# Better form building
gem 'simple_form'
gem 'ckeditor'

# Backend Handler
gem 'activeadmin'

gem 'devise'
gem 'cancancan'
gem 'draper'
gem 'pundit'

# For file upload
gem 'carrierwave'
gem 'mini_magick'
gem 'fog'
gem 'fog-aws'

# For reading and writing excel files
gem 'spreadsheet', require: false

# For setting config data
gem 'figaro'

# Friendly IDs
gem 'friendly_id'

# Apply CSS to Emails
gem 'premailer-rails'

# Pass Ruby data to JS
gem 'gon'

# Pass Routes to JS
gem 'js-routes'

# Generate json from model attributes
gem 'active_model_serializers', '~> 0.10.0'

# Detect browser client
gem 'browser'

# Money
gem 'money-rails'

# Awesome Print
gem 'awesome_print'

group :development do
  gem 'mina'

  gem 'letter_opener'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'seed_dump'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  gem 'pry-rails'
end
