# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180320075727) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "banner_links", force: :cascade do |t|
    t.string   "image"
    t.string   "link"
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "cars", force: :cascade do |t|
    t.string  "trim"
    t.string  "description"
    t.integer "year_id"
    t.integer "vehicle_make_id"
    t.integer "vehicle_model_id"
    t.string  "vehicle_type"
  end

  add_index "cars", ["vehicle_make_id"], name: "index_cars_on_vehicle_make_id", using: :btree
  add_index "cars", ["vehicle_model_id"], name: "index_cars_on_vehicle_model_id", using: :btree
  add_index "cars", ["year_id"], name: "index_cars_on_year_id", using: :btree

  create_table "cars_packages", id: false, force: :cascade do |t|
    t.integer "car_id",     null: false
    t.integer "package_id", null: false
  end

  add_index "cars_packages", ["car_id", "package_id"], name: "index_cars_packages_on_car_id_and_package_id", using: :btree
  add_index "cars_packages", ["package_id", "car_id"], name: "index_cars_packages_on_package_id_and_car_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["type"], name: "index_ckeditor_assets_on_type", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "logo"
    t.string   "link"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "contact_us_records", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.string   "subject"
    t.string   "message"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "coverage_handlers", force: :cascade do |t|
    t.float    "value"
    t.integer  "package_id"
    t.integer  "coverage_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "sort_order",  default: 0
  end

  add_index "coverage_handlers", ["coverage_id"], name: "index_coverage_handlers_on_coverage_id", using: :btree
  add_index "coverage_handlers", ["package_id"], name: "index_coverage_handlers_on_package_id", using: :btree

  create_table "coverages", force: :cascade do |t|
    t.string   "name"
    t.string   "value_type"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "priority",   default: 0
  end

  create_table "excel_storages", force: :cascade do |t|
    t.string   "file"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "status"
    t.string   "job_id"
    t.integer  "invalid_rows",              array: true
  end

  create_table "features", force: :cascade do |t|
    t.string   "icon"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "features_packages", id: false, force: :cascade do |t|
    t.integer "feature_id", null: false
    t.integer "package_id", null: false
  end

  add_index "features_packages", ["feature_id", "package_id"], name: "index_features_packages_on_feature_id_and_package_id", using: :btree
  add_index "features_packages", ["package_id", "feature_id"], name: "index_features_packages_on_package_id_and_feature_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "inquiries", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "service_id"
    t.integer  "car_id"
    t.integer  "package_id"
    t.string   "email"
    t.string   "name"
    t.string   "phone_number"
    t.string   "region"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "origin"
    t.text     "message"
  end

  add_index "inquiries", ["car_id"], name: "index_inquiries_on_car_id", using: :btree
  add_index "inquiries", ["package_id"], name: "index_inquiries_on_package_id", using: :btree
  add_index "inquiries", ["service_id"], name: "index_inquiries_on_service_id", using: :btree
  add_index "inquiries", ["user_id"], name: "index_inquiries_on_user_id", using: :btree

  create_table "packages", force: :cascade do |t|
    t.string   "name"
    t.string   "discount_type"
    t.float    "discount_value"
    t.text     "description"
    t.string   "repair_site",             default: "none"
    t.integer  "company_id"
    t.integer  "service_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "sum_insured_satangs",     default: 0,      null: false
    t.string   "sum_insured_currency",    default: "THB",  null: false
    t.integer  "excess_payment_satangs",  default: 0,      null: false
    t.string   "excess_payment_currency", default: "THB",  null: false
    t.integer  "actual_price_satangs",    default: 0,      null: false
    t.string   "actual_price_currency",   default: "THB",  null: false
    t.string   "package_identifier"
  end

  add_index "packages", ["company_id"], name: "index_packages_on_company_id", using: :btree
  add_index "packages", ["service_id"], name: "index_packages_on_service_id", using: :btree

  create_table "promotions", force: :cascade do |t|
    t.string   "image"
    t.string   "link"
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "service_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "identifier"
    t.string   "banner_image"
    t.string   "homepage_image"
    t.text     "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.integer  "service_category_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "icon"
    t.string   "link"
  end

  add_index "services", ["service_category_id"], name: "index_services_on_service_category_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "inquiry_generated",      default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicle_makes", force: :cascade do |t|
    t.string  "name"
    t.boolean "is_popular", default: false
  end

  create_table "vehicle_models", force: :cascade do |t|
    t.string  "name"
    t.integer "vehicle_make_id"
    t.boolean "is_popular",      default: false
  end

  add_index "vehicle_models", ["vehicle_make_id"], name: "index_vehicle_models_on_vehicle_make_id", using: :btree

  create_table "years", force: :cascade do |t|
    t.string "name"
  end

end
