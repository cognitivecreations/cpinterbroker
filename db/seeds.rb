# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
# From Folder
# 	Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }
#
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

insurance = ServiceCategory.create(name: 'ประกันภัยรถยนต์', identifier: 'insurance')
insurance.services.create(name: 'ประกันภัยรถยนต์ชั้น 1')
insurance.services.create(name: 'ประกันภัยรถยนต์ชั้น 2')
insurance.services.create(name: 'ประกันภัยรถยนต์ชั้น 3')
insurance.services.create(name: 'ประกันภัยรถยนต์ชั้น 2+')
insurance.services.create(name: 'ประกันภัยรถยนต์ชั้น 3+')

ServiceCategory.create(name: 'สินเชื่อรถยนต์', identifier: 'finance')
ServiceCategory.create(name: 'ทะเบียนและภาษีรถยนต์', identifier: 'tax')

insurance = ServiceCategory.create(name: 'ประกันภัย Non Motor', identifier: 'insurance-non-motor')
insurance.services.create(name: 'อัคคีภัย')
insurance.services.create(name: 'ขนส่ง')
insurance.services.create(name: 'อุบัติเหตุ')
insurance.services.create(name: 'เดินทาง')

Coverage.create(name: 'ประกันอุบัติเหตุส่วนบุคคล', value_type: Coverage::VALUE_TYPES[0])
Coverage.create(name: 'ค่ารักษาพยาบาล', value_type: Coverage::VALUE_TYPES[0])
Coverage.create(name: 'ประกันตัวผู้ขับขี่ คดีอาญา', value_type: Coverage::VALUE_TYPES[0])
Coverage.create(name: 'คุ้มครองผู้ขับขี่และผู้โดยสาร', value_type: Coverage::VALUE_TYPES[1])
Coverage.create(name: 'คุ้มครองทรัพย์สินของบุคคลภายนอก', value_type: Coverage::VALUE_TYPES[0])
Coverage.create(name: 'ความเสียหายต่อชีวิตร่างกาย หรืออนามัยของบุคคลภายนอก (ต่อคน)', value_type: Coverage::VALUE_TYPES[0])
Coverage.create(name: 'ความเสียหายต่อชีวิตร่างกาย หรืออนามัยของบุคคลภายนอก (ต่อครั้ง)', value_type: Coverage::VALUE_TYPES[0])

Feature.create(name: 'บริการช่วยเหลือ 24 ชม.')
Feature.create(name: 'รถสำรองใช้ระหว่างซ่อม')
Feature.create(name: 'ประกันไฟไหม้และการโจรกรรม')
Feature.create(name: 'คุ้มครองน้ำท่วม')

# Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }
