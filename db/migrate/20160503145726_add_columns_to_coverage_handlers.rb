class AddColumnsToCoverageHandlers < ActiveRecord::Migration
  def change
    add_column :coverage_handlers, :sort_order, :integer, default: 0
  end
end
