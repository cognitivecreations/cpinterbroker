class AddMessageColumnToInquiries < ActiveRecord::Migration
  def change
    add_column :inquiries, :message, :text
  end
end
