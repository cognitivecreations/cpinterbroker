class CreateCoverageHandlers < ActiveRecord::Migration
  def change
    create_table :coverage_handlers do |t|
      t.float :value
      t.references :package, index: true
      t.references :coverage, index: true

      t.timestamps null: false
    end
  end
end
