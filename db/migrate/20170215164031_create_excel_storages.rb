class CreateExcelStorages < ActiveRecord::Migration
  def change
    create_table :excel_storages do |t|
      t.string :file

      t.timestamps null: false
    end
  end
end
