class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.string :image
      t.references :service_category, index: true

      t.timestamps null: false
    end
  end
end
