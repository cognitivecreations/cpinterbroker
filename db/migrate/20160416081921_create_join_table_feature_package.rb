class CreateJoinTableFeaturePackage < ActiveRecord::Migration
  def change
    create_join_table :features, :packages do |t|
      t.index [:feature_id, :package_id]
      t.index [:package_id, :feature_id]
    end
  end
end
