class AddDefaultValueToRepairSiteInPackages < ActiveRecord::Migration
  def change
    change_column_default :packages, :repair_site, 'none'
  end
end
