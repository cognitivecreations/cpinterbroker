class AddInquiryGeneratedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :inquiry_generated, :boolean, default: false
  end
end
