class ChangePackageValuesToMoney < ActiveRecord::Migration
  def change
    add_monetize :packages, :sum_insured
    add_monetize :packages, :excess_payment
    add_monetize :packages, :actual_price

    remove_column :packages, :sum_insured
    remove_column :packages, :excess_payment
    remove_column :packages, :actual_price
  end
end
