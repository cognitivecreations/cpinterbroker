class AddOriginToInquiries < ActiveRecord::Migration
  def change
    add_column :inquiries, :origin, :string
  end
end
