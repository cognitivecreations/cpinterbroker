class AddErrorHandlingColumnsToExcelStorage < ActiveRecord::Migration
  def change
    add_column :excel_storages, :status, :string
    add_column :excel_storages, :job_id, :string
    add_column :excel_storages, :invalid_rows, :integer, array: true
  end
end
