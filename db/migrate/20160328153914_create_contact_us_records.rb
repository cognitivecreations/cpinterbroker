class CreateContactUsRecords < ActiveRecord::Migration
  def change
    create_table :contact_us_records do |t|
      t.string :name
      t.string :email
      t.string :phone_number
      t.string :subject
      t.string :message

      t.timestamps null: false
    end
  end
end
