class AddVehicleTypeToCars < ActiveRecord::Migration
  def change
    add_column :cars, :vehicle_type, :string
  end
end
