class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string     :trim
      t.string     :description
      t.references :year, index: true
      t.references :vehicle_make, index: true
      t.references :vehicle_model, index: true
    end
  end
end
