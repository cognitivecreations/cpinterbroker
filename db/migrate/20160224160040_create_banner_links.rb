class CreateBannerLinks < ActiveRecord::Migration
  def change
    create_table :banner_links do |t|
      t.string :image
      t.string :link
      t.boolean :active, default: true

      t.timestamps null: false
    end
  end
end
