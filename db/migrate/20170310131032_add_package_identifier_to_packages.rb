class AddPackageIdentifierToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :package_identifier, :string
  end
end
