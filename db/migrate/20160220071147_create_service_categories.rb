class CreateServiceCategories < ActiveRecord::Migration
  def change
    create_table :service_categories do |t|
      t.string :name
      t.string :slug, unique: true
      t.string :identifier
      t.string :banner_image
      t.string :homepage_image
      t.text   :description

      t.timestamps null: false
    end
  end
end
