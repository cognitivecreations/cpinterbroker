class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string  :image
      t.string  :link
      t.boolean :active, default: true

      t.timestamps null: false
    end
  end
end
