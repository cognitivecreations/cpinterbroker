class CreateCoverages < ActiveRecord::Migration
  def change
    create_table :coverages do |t|
      t.string :name
      t.string :value_type

      t.timestamps null: false
    end
  end
end
