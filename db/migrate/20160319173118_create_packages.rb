class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string  :name
      t.float   :sum_insured
      t.float   :excess_payment
      t.float   :actual_price
      t.string  :discount_type
      t.float   :discount_value
      t.text    :description
      t.string  :repair_site
      t.references :company, index: true
      t.references :service, index: true

      t.timestamps null: false
    end
  end
end
