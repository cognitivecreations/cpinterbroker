class CreateJoinTableCarPackage < ActiveRecord::Migration
  def change
    create_join_table :cars, :packages do |t|
      t.index [:car_id, :package_id]
      t.index [:package_id, :car_id]
    end
  end
end
