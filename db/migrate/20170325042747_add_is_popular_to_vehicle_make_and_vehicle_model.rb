class AddIsPopularToVehicleMakeAndVehicleModel < ActiveRecord::Migration
  def change
    add_column :vehicle_makes, :is_popular, :boolean, default: false
    add_column :vehicle_models, :is_popular, :boolean, default: false
  end
end
