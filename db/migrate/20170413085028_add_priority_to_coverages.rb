class AddPriorityToCoverages < ActiveRecord::Migration
  def change
    add_column :coverages, :priority, :integer, default: 0
  end
end
