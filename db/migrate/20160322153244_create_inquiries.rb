class CreateInquiries < ActiveRecord::Migration
  def change
    create_table :inquiries do |t|
      t.references :user, index: true
      t.references :service, index: true
      t.references :car, index: true
      t.references :package, index: true
      t.string :email
      t.string :name
      t.string :phone_number
      t.string :region

      t.timestamps null: false
    end
  end
end
