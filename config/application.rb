require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Cpinterbroker
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.autoload_paths += Dir[Rails.root.join('lib', 'core_ext', '**', '*.rb').to_s].each { |l| require l }
    config.autoload_paths += Dir[Rails.root.join('lib', 'deep_struct', '**', '*.rb').to_s].each { |l| require l }
    config.autoload_paths += Dir[Rails.root.join('lib', 'excel', '**', '*.rb').to_s].each { |l| require l }

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Load namespaced serializers
    config.autoload_paths += Dir[Rails.root.join('app', 'serializers', '**', '*.rb').to_s]

    # Load namespaced jobs
    config.autoload_paths += Dir[Rails.root.join('app', 'jobs', '**', '*.rb').to_s]

    config.to_prepare do
      Devise::Mailer.layout 'mailer'
    end
  end
end
