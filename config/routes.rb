Rails.application.routes.draw do
  require 'sidekiq/web'
  require 'sidekiq-status/web'

  authenticate :admin_user do
    mount Sidekiq::Web => '/sidekiq'
    mount Ckeditor::Engine => '/ckeditor'
  end

  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get 'contact-us', to: 'contacts#index', as: 'contacts'
  post 'submit-contact', to: 'contacts#create', as: 'create_contact'

  get 'about-us', to: 'about_us#index', as: 'about_us'

  get 'vehicle_trims', to: 'cars#trims'
  get 'vehicle_makes', to: 'vehicle_makes#index'
  get 'vehicle_models', to: 'vehicle_models#index'
  get 'vehicle_models_active_admin', to: 'vehicle_models#active_admin_index'

  root 'main#index'

  resources :inquiries, only: [:create]

  resources :service_categories, path: '/' do
    member do
      get 'calculator'
      get 'results'
      get 'summary'
    end
  end

  get '/excel_storages/:id/progress', to: 'excel_storages#progress'

  scope :api do
    resources :packages, only: :index
  end
end
