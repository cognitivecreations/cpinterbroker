CarrierWave.configure do |config|
  config.storage = :fog
  config.fog_credentials = {
    provider:              'AWS',
    aws_access_key_id:     ENV['AWS_ACCESS_KEY'],
    aws_secret_access_key: ENV['AWS_SECRET_KEY'],
    region:                ENV['AWS_REGION'],
    endpoint:              'https://sgp1.digitaloceanspaces.com'
  }
  config.fog_directory =   ENV['AWS_BUCKET_NAME']
end
