class ExcelRow
  attr_accessor :columns

  def initialize(data)
    self.columns = data.map{ |d| d.is_a?(String) ? d.strip : d }
    puts self.columns.inspect
  end

  def package_identifier
    self.columns[0]
  end

  def package_name
    self.columns[7]
  end

  def sum_insured
    self.columns[12]
  end

  def excess_payment
    self.columns[15]
  end

  def actual_price
    self.columns[11]
  end

  def discount_type
    self.columns[13] if self.columns[13].present? and self.columns[14].present? and Package::DISCOUNT_TYPES.include?(self.columns[13])
  end

  def discount_value
    if discount_type
      self.columns[14]
    end
  end

  def description
    self.columns[10]
  end

  def repair_site
    Package::REPAIR_SITES[Package::REPAIR_SITES_TH.index(self.columns[9].split(","))] if self.columns[9].present?
  end

  def company_id
    Company.find_or_create_by(name: self.columns[1].strip).id
  end

  def service_id
    serv_id = self.columns[8]
    serv_id = serv_id.to_i.to_s if !(serv_id.is_a? String)
    Service.find_by(name: "ประกันภัยรถยนต์ชั้น #{serv_id}").id if serv_id.present? and Package::SERVICE_LEVELS.include?(serv_id)
  end

  def car_ids
    trims = self.columns[5]
    vehicle_types = self.columns[6]
    cars = Car.where(year_id: self.car_year, vehicle_make_id: self.car_make, vehicle_model_id: self.car_model)
    cars = self.generate_query(trims, cars, 'trim', true)
    cars = self.generate_query(vehicle_types, cars, 'vehicle_type')
    cars.pluck(:id)
  end

  def car_year
    # puts self.columns[2]
    text = self.columns[2]
    list = []
    ignore_list = []

    if  text.is_a? Float
      text = text.to_i.to_s
    end

    splitted_text = text.strip.split(',')
    # puts "splitted_text: #{splitted_text}"

    splitted_text.each do |t|
      if t == '*'
        # puts "adding all: #{t}"
        list += Year.pluck(:name)
      else
        q = t.split('-')
        if q.first.blank?
          if q.length > 2
            ignore_list += (q[1].to_i..q[2].to_i).to_a.map(&:to_s)
          else
            ignore_list += [q[1]]
          end
        else
          # puts "here #{q}"
          if q.length == 2
            list += (q[0].to_i..q[1].to_i).to_a.map(&:to_s)
          else
            list += [q[0]]
          end
        end
      end
    end

    list = list.uniq
    ignore_list = ignore_list.uniq
    resulting_list = list - ignore_list
    result_ids = []

    # puts list.inspect
    # puts ignore_list.inspect

    resulting_list.each do |l|
      result_ids << Year.find_or_create_by(name: l).id
    end

    # puts result_ids.inspect

    result_ids
  end

  def car_make
    self.generate_query(columns[3], 'vehicle_make', 'name').pluck(:id)
  end

  def car_model
    self.generate_query(columns[4], 'vehicle_model', 'name').pluck(:id)
  end

  def generate_query(text, klass_or_instance, column_name, partial_match = false)
    splitted_text = text.split(',')
    and_list = []
    or_list = []
    splitted_text.map(&:strip).each do |t|
      unless t == '*'
        if t.start_with? '-'
          if partial_match
            and_list << "#{column_name} not ilike '%#{t.sub('-', '')}%'"
          else
            and_list << "#{column_name} not ilike '#{t.sub('-', '')}'"
          end
        else
          if partial_match
            or_list << "#{column_name} ilike '%#{t}%'"
          else
            or_list << "#{column_name} ilike '#{t}'"
          end
        end
      end
    end.compact

    query = ""
    query += "(" if or_list.present?
    query += or_list.join(' OR ')
    query += ")" if or_list.present?
    query += " OR " if or_list.present? and and_list.present?
    query += "(" if and_list.present?
    query += and_list.join(' AND ')
    query += ")" if and_list.present?

    puts query.inspect

    if klass_or_instance.is_a? String
      klass_or_instance.camelize.constantize.where(query)
    elsif klass_or_instance.is_a? Car::ActiveRecord_Relation
      klass_or_instance.where(query)
    end
  end

  def feature_ids
    ids = []
    ids << Feature.find_by(name: 'คุ้มครองน้ำท่วม').id if self.columns[16].present? and self.columns[16] == 1.0
    ids << Feature.find_by(name: 'ประกันไฟไหม้และการโจรกรรม').id if self.columns[17].present? and self.columns[17] == 1.0
    ids << Feature.find_by(name: 'รถสำรองใช้ระหว่างซ่อม').id if self.columns[18].present? and self.columns[18] == 1.0
    ids << Feature.find_by(name: 'บริการช่วยเหลือ 24 ชม.').id if self.columns[19].present? and self.columns[19] == 1.0
    ids
  end

  def coverage_handlers
    c = []
    if self.columns[20].present?
      id = Coverage.find_by(name: 'ประกันอุบัติเหตุส่วนบุคคล').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[20])
    end
    if self.columns[21].present?
      id = Coverage.find_by(name: 'ค่ารักษาพยาบาล').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[21])
    end
    if self.columns[22].present?
      id = Coverage.find_by(name: 'ประกันตัวผู้ขับขี่ คดีอาญา').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[22])
    end
    if self.columns[23].present?
      id = Coverage.find_by(name: 'คุ้มครองผู้ขับขี่และผู้โดยสาร').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[23])
    end
    if self.columns[24].present?
      id = Coverage.find_by(name: 'คุ้มครองทรัพย์สินของบุคคลภายนอก').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[24])
    end
    if self.columns[25].present?
      id = Coverage.find_by(name: 'ความเสียหายต่อชีวิตร่างกาย หรืออนามัยของบุคคลภายนอก (ต่อคน)').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[25])
    end
    if self.columns[26].present?
      id = Coverage.find_by(name: 'ความเสียหายต่อชีวิตร่างกาย หรืออนามัยของบุคคลภายนอก (ต่อครั้ง)').id
      c << CoverageHandler.new(coverage_id: id, value: self.columns[26])
    end
    c
  end
end
