require 'json'
require 'open-uri'
require 'rack/utils'

namespace :data do
  task :cars, [:start_year, :finish_year] => [:environment] do |t, args|
    args.with_defaults(start_year: 1992, finish_year: 2017)
    puts "Populating cars from year #{args[:start_year]} to #{args[:finish_year]}"

    (args[:start_year].to_i..args[:finish_year].to_i).each do |year|
      puts year
      @year = Year.find_or_create_by(name: year)

      brands = JSON.parse(open("https://finance.rabbit.co.th/car-insurance/get-brands-by-year?year=#{year}").read)
      brands.each do |brand|
        @brand = VehicleMake.where('lower(name) = ?', brand["name"].downcase).first_or_create!(name: brand["name"])

        models = JSON.parse(open("https://finance.rabbit.co.th/car-insurance/get-models-by-brand-id-and-year?year=#{year}&id=#{brand['id']}").read)
        models.each do |model|
          @model = @brand.vehicle_models.where('lower(name) = ?', model["name"].downcase).first_or_create!(name: model["name"])

          submodels = JSON.parse(open("https://finance.rabbit.co.th/car-insurance/get-submodels-by-model-id-and-year?year=#{year}&id=#{model['id']}").read)
          submodels.each do |submodel|
            @car = @model.cars.where('lower(trim) = ? AND year_id = ?', submodel["name"].downcase, @year.id).first_or_create!(year: @year, trim: submodel["name"], vehicle_make: @brand)

            puts @car.inspect
          end
        end
      end
    end
  end

  task :companies => [:environment] do |t|
    puts "Populating Companies"
    import_data("company")
  end

  task :coverages => [:environment] do |t|
    puts "Populating Coverages"
    import_data("coverage")
  end

  task :features => [:environment] do |t|
    puts "Populating Features"
    import_data("feature")
  end

  task :packages, [:count] => [:environment] do |t, args|
    args.with_defaults(count: 1000)
    puts "Populating #{args[:count]} Packages"

    1.upto(args[:count].to_i).each do |index|
      package = Package.new
      package.name = "Package #{index}"
      package.company = Company.order("RANDOM()").first
      package.service = Service.order("RANDOM()").first
      package.features << Feature.all
      package.sum_insured = rand.round(2) * 1000000
      package.discount_type = [:percentage, :amount].sample
      package.discount_value = (package.discount_type == "percentage" ? rand.round(2) * 20 : rand.round(2) * 10000)
      package.actual_price = rand.round(2) * 100000
      package.excess_payment = rand.round(2) * 10000
      package.cars << Car.order("RANDOM()").first(3)
      Coverage.all.each do |coverage|
        package.coverage_handlers << [CoverageHandler.new(coverage: coverage, value: (coverage.value_type == "amount" ? rand.round(2) * 100000 : (rand * 10).to_i))]
      end
      package.save
    end
  end

  task :populate_all, [:list] => [:environment] do |t, args|
    args.with_defaults(list: %w(companies features coverages cars packages))
    puts "Populating #{args[:list]}"

    args[:list].each do |l|
      Rake::Task["data:#{l}"].invoke
    end
  end

  task reset_cars: :environment do
    [Year, VehicleModel, VehicleMake, Car].each do |model|
      model.delete_all
      ActiveRecord::Base.connection.execute(
        "ALTER SEQUENCE #{model.table_name}_id_seq RESTART WITH 1"
      )
    end
  end

  task :import_cars_from_csv => [:environment] do |t, args|
    require 'csv'

    CSV.foreach("lib/data/cars.csv") do |row|
      year = Year.find_or_create_by(name: row[0].strip)
      vehicle_make = VehicleMake.where('lower(name) = ?', row[1].strip.downcase).first_or_create!(name: row[1].strip)
      vehicle_model = vehicle_make.vehicle_models.where('lower(name) = ?', row[2].strip.downcase).first_or_create!(name: row[2].strip)
      car = vehicle_model.cars.where('lower(trim) = ? AND lower(vehicle_type) = ? AND year_id = ?', row[3].strip.downcase, row[4].strip.downcase, year.id).first_or_create!(year: year, trim: row[3].strip, vehicle_make: vehicle_make, vehicle_type: row[4].strip)
      puts car.inspect
    end
  end
end

def import_data(klass)
  data = JSON.parse(File.read("lib/data/#{klass.pluralize}.json"))[klass.pluralize]
  klass.capitalize.constantize.create!(data)
end
