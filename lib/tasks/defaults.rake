namespace :defaults do
  task packages: :environment do
    Package.where(repair_site: nil).update_all(repair_site: 'none')
  end
end