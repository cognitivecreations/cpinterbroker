class String
  def chomp_left(to_chomp)
    self.reverse.chomp(to_chomp.reverse).reverse
  end
end
