class ApplicationMailer < ActionMailer::Base
  add_template_helper MailerHelper
  default from: ENV['EMAIL_ADDRESS']
  layout 'mailer'
end
