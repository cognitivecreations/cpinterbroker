class InquiryMailer < ApplicationMailer
  def inquiry_email(inquiry = nil)
    @inquiry = inquiry
    mail(to: @inquiry.email, subject: 'Sample Email')
  end
end
