class ContactMailer < ApplicationMailer
  def contact_email(contact)
    @contact = contact
    @title = t('mailer.recieved_email', name: @contact.name)
    mail(to: ENV['EMAIL_ADDRESS'], subject: "[CONTACT] #{@contact.subject}", from: @contact.email)
  end
end
