class UserMailer < ApplicationMailer
  def password_setup(user)
    @user = user
    @title = t('mailer.welcome')

    @reset_password_token = @user.generate_reset_password_token

    mail(to: @user.email, subject: t('mailer.welcome'))
  end
end
