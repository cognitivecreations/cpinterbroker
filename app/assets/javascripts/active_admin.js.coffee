#= require active_admin/base
#= require selectize
#= require upload_progress
#= require package_cars
#= require ckeditor/init

$(document).ready ->
  $('select[data-option-dependent=true]').each (i) ->
    observer_dom_id = $(this).attr('id')
    observed_dom_id = $(this).data('option-observed')
    url_mask = $(this).data('option-url')
    key_method = 'id'
    #$(this).data('option-key-method');
    value_method = 'name'
    #$(this).data('option-value-method');
    prompt = if $(this).has('option[value]').size() then $(this).find('option[value]') else $('<option>').text('?')
    regexp = /:[0-9a-zA-Z_]+/g
    observer = $('select#' + observer_dom_id)
    observed = $('select#' + observed_dom_id)
    if !observer.val() and observed.size() > 1
      observer.attr 'disabled', true
    observed.on 'change', ->
      url = url_mask.replace(regexp, (submask) ->
        dom_id = submask.substring(1, submask.length)
        $('select#' + dom_id).val()
      )
      observer.empty().append prompt
      $.getJSON url, (data) ->
        $.each data, (i, object) ->
          observer.append $('<option>').attr('value', object[key_method]).text(object[value_method])
          observer.attr 'disabled', false
          return
        return
      return
    return

  $('select:not(.no-selectize)').selectize()

  if $('#ckeditor').length
    CKEDITOR.replace( 'ckeditor' )

  if $('#ckeditor').prev('label').length
    $('#ckeditor').prev('label').css('float','none')
