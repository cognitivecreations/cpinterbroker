var VuePackage = {
  template: '#package-item',
  props: {
    package: Object,
    features: Array
  },
  data: function () {
    return {
      selected: false,
      expanded: false
    }
  },
  computed: {
    panelClass: function () {
      return {
        selected: this.selected
      }
    },
    link: function () {
      return '/insurace/summary?package_id=' + this.package.id + '&car_id=' + gon.results.car.id + '&region=' + gon.results.region
    },
    expandBtnText: function () {
      return this.expanded ? 'ซ่อนรายละเอียด' : 'ดูรายละเอียด'
    }
  },
  methods: {
    toggleSelected: function () {
      this.selected = !this.selected
      this.$emit('selected', this.package.id, this.selected)
    },
    featureClass: function (feature) {
      return {
        selected: _.includes(_.map(this.package.features, 'id'), feature.id)
      }
    }
  }
}
