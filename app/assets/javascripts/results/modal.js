var VueModal = {
  template: '#modal',
  props: ['visible', 'packages'],
  watch: {
    visible: function (value) {
      $(this.$refs.modal).modal(value ? 'show' : 'hide')
    }
  },
  methods: {
    link: function (pkg) {
      return '/insurace/summary?package_id=' + pkg.id + '&car_id=' + gon.results.car.id + '&region=' + gon.results.region
    },

    close: function () {
      this.visible = false
      this.$emit('close')
    }
  },
  mounted: function () {
    $('[data-toggle="tooltip"]').tooltip()
  }
}
