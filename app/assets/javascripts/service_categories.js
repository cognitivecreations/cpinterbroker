//= require calculator
//= require results

document.addEventListener("turbolinks:load", function () {
  if (!_.isEmpty($('.calculator'))) {
    Calculator.init()
  }

  if (!_.isEmpty($('.results'))) {
    Results.init()
  }
})
