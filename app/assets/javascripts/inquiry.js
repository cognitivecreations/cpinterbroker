//= require lodash
//= require jquery.serializejson.min

var Inquiry = {
  title: 'กรุณากรอกข้อมูลการติดต่อของคุณ',
  fields: ['email', 'phone-number'],
  url: '/inquiries',
  inputs: [],
  values: {
    car_id: null,
    region: null,
  },
  hasCloseButton: true,

  init: function (options) {
    options = options || {}
    this.clearFields()
    this.setTitle(options.title)
    this.setFields(options.fields)
    this.setUrl(options.url)
    this.setCarId(options.carId)
    this.setRegion(options.region)
    this.setHasCloseButton(options.hasCloseButton)

    if (this.hasCloseButton) {
      $('#inquiry-modal').modal('show')
      $('#inquiry-modal-close-button').removeClass('hidden')
    } else {
      $('#inquiry-modal-close-button').addClass('hidden')
      $('#inquiry-modal').modal({
        backdrop: 'static',
        keyboard: false
      })
    }

    $('#inquiry-modal').on('click', '#submit-button', function (e) {
      e.preventDefault();
      Inquiry.submit();
    })
  },

  clearFields: function () {
    this.values = {
      car_id: this.values.carId,
      region: this.values.region,
    }
    _.each($('#inquiry-modal').find('input'), function (input) {
      $(input).val('');
    })
  },

  setTitle: function (title) {
    if (!_.isUndefined(title) && _.isString(title))
      this.title = title;

    $('#modal-title').text(this.title);
  },

  setFields: function (fields) {
    if (!_.isUndefined(fields) && _.isArray(fields))
      this.fields = fields;

    _.each(this.fields, function (field) {
      Inquiry.inputs.push($('#inquiry-' + field));
      $('#inquiry-' + field).parents('.form-group').removeClass('hidden');
    })
  },

  setUrl: function (url) {
    if (!_.isUndefined(url) && _.isString(url))
      this.url = url;
  },

  setCarId: function (carId) {
    if (!_.isUndefined(carId) && _.isInteger(carId))
      this.values.car_id = carId;
  },

  setRegion: function (region) {
    if (!_.isUndefined(region) && _.isString(region))
      this.values.region = region;
  },

  setHasCloseButton: function (hasCloseButton) {
    if (!_.isUndefined(hasCloseButton) && _.isBoolean(hasCloseButton))
      this.hasCloseButton = hasCloseButton;
  },

  submit: function () {
    if (this.validate()) {
      $.post(this.url, this.values)
      .done(function (response) {
        console.log(response)
        $('#inquiry-modal').modal('hide')
      })
      .error(function (response) {
        console.error(response)
      })
    }
  },

  validate: function () {
    var email = $('#inquiry-email').val()
    var phoneNumber = $('#inquiry-phone-number').val()
    Inquiry.values.email = email
    Inquiry.values.phone_number = phoneNumber

    $('.validation').addClass('hidden')
    var result = true
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var rpN = /^0[1896]\d{8}$/;
    if (!re.test(email)){
      $('.validation#validation-email').removeClass('hidden')
      result = false
    }
    if (!rpN.test(phoneNumber)) {
      $('.validation#validation-phone-number').removeClass('hidden')
      result = false
    }
    return result
  }
}
