$(document).ready(function () {
  $.each($('td.col.col-progress span'), function (index, $el) {
    var id =  $(this).text()
    var url = "/excel_storages/" + id + "/progress"
    getProgress(url, id)
  })
})

function getProgress(url, id) {
  $.get(url)
  .done(function (res) {
    if (res == null || res.progress == null) return
    if (res.progress < 100) {
      $('tr#excel_storage_' + id + ' td.col.col-progress').text(res.progress + "%")
      setTimeout(function () {
        getProgress(url, id)
      }, 5000)
    }
    else {
      window.location.reload()
    }
  })
}
