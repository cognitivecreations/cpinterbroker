//= require selectize
//= require lodash
//= require js-routes
//= require jquery.serializejson.min
//= require rivets/rivets.bundled.min.js

var rvBinding = null

var Calculator = {
  xhr: null,

  data: {
    updater: 0,
    class_images: [],
    sort: {
      company: 'asc',
      service: 'asc',
      sum_insured: 'asc',
      excess_payment: 'asc',
      actual_price: 'asc'
    },
    filters: [1, 2, 3, 4, 5],

    toggleSort: function(el, rv) {
      var sortColumn = $(this).data('sort-column')
      rv.data.results = _.orderBy(
        rv.data.results,
        function(r) {
          if (sortColumn == 'company' || sortColumn == 'service')
            return r[sortColumn].name
          return r[sortColumn]
        },
        Calculator.data.sort[sortColumn]
      )

      if (Calculator.data.sort[sortColumn] == 'asc') {
        Calculator.data.sort[sortColumn] = 'desc'
      } else {
        Calculator.data.sort[sortColumn] = 'asc'
      }

      toggleCaret(this, sortColumn)
    },

    inquire: function(el, rv) {
      window.location = Routes.summary_service_category_path('insurance', {
        package_id: rv.result.id,
        car_id: gon.results.car.id
      })
    }
  },

  postData: {},

  selectize: function(selector, onChange) {
    return $(selector).selectize({
      valueField: 'id',
      labelField: 'name',
      searchField: ['name'],
      onChange: onChange
    })
  },

  selectizeOnChange: function(value, nextSelect, uri) {
    if (!value.length) return

    nextSelect.disable()
    nextSelect.clearOptions()

    nextSelect.load(function(callback) {
      Calculator.xhr && Calculator.xhr.abort()
      Calculator.xhr = $.getJSON(uri.url, uri.params).success(function(
        results
      ) {
        nextSelect.enable()
        callback(results)
      })
    })
  },

  setupSelectFields: function() {
    Calculator.year = Calculator.selectize('#calculate_year', function(value) {
      Calculator.selectizeOnChange(value, Calculator.make[0].selectize, {
        url: '/vehicle_makes.json',
        params: {
          year_id: value
        }
      })
    })

    Calculator.make = Calculator.selectize('#calculate_vehicle_make', function(
      value
    ) {
      Calculator.selectizeOnChange(value, Calculator.model[0].selectize, {
        url: '/vehicle_models.json',
        params: {
          year_id: Calculator.year.val(),
          vehicle_make_id: value
        }
      })
    })

    Calculator.model = Calculator.selectize(
      '#calculate_vehicle_model',
      function(value) {
        Calculator.selectizeOnChange(value, Calculator.trim[0].selectize, {
          url: '/vehicle_trims.json',
          params: {
            year_id: Calculator.year.val(),
            vehicle_make_id: Calculator.make.val(),
            vehicle_model_id: value
          }
        })
      }
    )

    Calculator.trim = Calculator.selectize('#calculate_vehicle_trim')

    Calculator.make[0].selectize.disable()
    Calculator.model[0].selectize.disable()
    Calculator.trim[0].selectize.disable()
  },

  setupHandlers: function() {
    $('form.calculate').submit(function(e) {
      var calculate = $(this).serializeJSON().calculate
      var valid = !_.includes(calculate, '')

      if (valid) {
        $('#loader').removeClass('hidden')
        $('#loader-text').addClass('hidden')
        $('#loading-text').removeClass('hidden')
      } else {
        e.preventDefault()
        e.stopPropagation()
        return false
      }
    })
  },

  setUpDetailsHandlers: function() {
    function collapseDetails($selection) {
      $selection.children('div').slideUp(500, 'jswing', function(e) {
        $selection.addClass('collapsed')
      })
    }

    function expandDetails($selection) {
      $selection.removeClass('collapsed')
      collapseDetails($('.details').not($selection))
      $selection.children('div').slideDown(500, 'jswing')
    }

    $('#results').on('click', '.btn-details', function(e) {
      $details = $(this)
        .parents('tr')
        .siblings()
        .find('.details')

      if ($details.hasClass('collapsed')) {
        expandDetails($details)
      } else {
        collapseDetails($details)
      }
    })
  },

  setupCheckboxHandlers: function() {
    $('#results').on('click', '.clickable', function(e) {
      if ($(e.toElement).is('td')) {
        $(this)
          .find('.select_box')
          .trigger('click')
      }
    })

    $('#results').on('change', '.select_box', function(e) {
      Calculator.data.updater++

      if (
        $('.select_box:checked').length > 1 &&
        $('.select_box:checked').length < 4
      ) {
        $('#compare').fadeIn('fast')
      } else {
        $('#compare').fadeOut('fast')
      }

      if ($('.select_box:checked').length == 2) {
        $('.compare-modal .insurance-header').removeClass(
          'insurance-header-three'
        )
        $('.compare-modal .insurance-header').addClass('insurance-header-two')
      } else if ($('.select_box:checked').length == 3) {
        $('.compare-modal .insurance-header').removeClass(
          'insurance-header-two'
        )
        $('.compare-modal .insurance-header').addClass('insurance-header-three')
      }
    })
  },

  setupRivets: function() {
    Calculator.data.results = []

    rivets.formatters.display = function(value, type) {
      return type == 'amount' ? '฿' + value : value + ' คน'
    }

    rivets.formatters.half = function(array, setIndex) {
      array = _.sortBy(array, 'sort_order')
      return setIndex == 0
        ? array.slice(0, Math.ceil(array.length / 2))
        : array.slice(array.length / 2, array.length)
    }

    rivets.formatters.numeral = function(value, formatValue) {
      return numeral(value).format(formatValue)
    }

    rivets.formatters.filter = function(array, serviceIds) {
      if (_.isArray(serviceIds)) {
        return _.filter(array, function(item) {
          return _.includes(serviceIds, item.service.id)
        })
      } else {
        return _.filter(array, function(item) {
          return serviceIds == item.service.id
        })
      }
    }

    rivets.formatters.includes = function(array, filter) {
      console.log('includes', array, filter)
      return _.filter(array, function(item) {
        return item == filter
      })
    }

    rivets.formatters.without = function(array, filter) {
      console.log('without', array, filter)
      return !_.filter(array, function(item) {
        return item == filter
      })
    }

    rivets.formatters.capitalize = function(value) {
      return value.charAt(0).toUpperCase() + value.slice(1)
    }

    rivets.formatters.discount = function(value, type) {
      return type == 'amount' ? '฿' + value : value + '%'
    }

    rivets.formatters.length = function(array) {
      return array ? array.length : 0
    }

    rivets.formatters.attr = function(obj, key) {
      return obj[key]
    }

    rivets.formatters.checked = function(array) {
      return _.filter(array, { checked: true })
    }

    rvBinding = rivets.bind($('.rivets-container'), { data: Calculator.data })
  },

  init: function() {
    Calculator.setupSelectFields()
    Calculator.setupHandlers()
    Calculator.setupRivets()
  }
}

function toggleCaret(selector, sortColumn) {
  //clear existing carets (if any)
  $('.results-header thead a')
    .find('i')
    .remove()

  //reverse order for insurance class lvl
  if (sortColumn == 'service') {
    Calculator.data.sort[sortColumn] == 'asc'
      ? $(selector).append(' <i class="fa fa-caret-up"></i>')
      : $(selector).append(' <i class="fa fa-caret-down"></i>')
  } else {
    Calculator.data.sort[sortColumn] == 'asc'
      ? $(selector).append(' <i class="fa fa-caret-down"></i>')
      : $(selector).append(' <i class="fa fa-caret-up"></i>')
  }
}
