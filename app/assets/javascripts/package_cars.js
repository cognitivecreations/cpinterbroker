$(document).ready(function () {
  $('#package_car_ids').selectize({
    labelField: 'as_string',
    valueField: 'id',
    searchField: 'as_string',
    load: function (query, callback) {
      $.get('/admin/cars.json?q[year_name_or_vehicle_make_name_or_vehicle_model_name_or_trim_cont]=' + query)
      .then(function (response) {
        callback(response)
      })
    }
  })
})
