//= require selectize
//= require lodash
//= require jquery.serializejson.min

var Calculator = {
  xhr: null,

  init: function () {
    Calculator.setupSelectFields()
    Calculator.setupHandlers()
  },

  selectize: function (selector, onChange) {
    return $(selector).selectize({
      valueField: 'id',
      labelField: 'name',
      searchField: ['name'],
      onChange: onChange
    });
  },

  selectizeOnChange: function (value, nextSelectors, uri) {
    if (!value.length) return

    var nextSelect = Calculator[nextSelectors[0]][0].selectize

    _.each(nextSelectors, function (name) {
      var nextSelector = Calculator[name][0].selectize
      nextSelector.disable()
      nextSelector.clearOptions()
    })

    nextSelect.load(function (callback) {
      Calculator.xhr && Calculator.xhr.abort()
      Calculator.xhr = $.getJSON(uri.url, uri.params)
        .success(function (results) {
          nextSelect.enable()
          callback(results)
        })
    })
  },

  setupSelectFields: function () {
    Calculator.year = Calculator.selectize('#calculate_year', function (value) {
      Calculator.selectizeOnChange(value, ["make", "model", "trim"], {
        url: '/vehicle_makes.json',
        params: {
          year_id: value
        }
      })
    })

    Calculator.make = Calculator.selectize('#calculate_vehicle_make', function (value) {
      Calculator.selectizeOnChange(value, ["model", "trim"], {
        url: '/vehicle_models.json',
        params: {
          year_id: Calculator.year.val(),
          vehicle_make_id: value
        }
      })
    })

    Calculator.model = Calculator.selectize('#calculate_vehicle_model', function (value) {
      Calculator.selectizeOnChange(value, ["trim"], {
        url: '/vehicle_trims.json',
        params: {
          year_id: Calculator.year.val(),
          vehicle_make_id: Calculator.make.val(),
          vehicle_model_id: value,
        }
      })
    })

    Calculator.trim = Calculator.selectize('#calculate_vehicle_trim')

    Calculator.make[0].selectize.disable()
    Calculator.model[0].selectize.disable()
    Calculator.trim[0].selectize.disable()
  },

  setupHandlers: function () {
    $('form.calculate').submit(function (e) {
      var calculate = $(this).serializeJSON().calculate
      var valid = !_.includes(_.omit(calculate, 'service_id'), '')

      if (valid) {
        $('#loader').removeClass('hidden')
        $('#loader-text').addClass('hidden')
        $('#loading-text').removeClass('hidden')
      } else {
        e.preventDefault()
        e.stopPropagation()
        return false
      }
    })
  }
}
