document.addEventListener('turbolinks:load', function() {
  $('.promotions-carousel').owlCarousel({
    loop: true,
    margin: 10,
    dotsEach: 1,
    responsive: {
      0: {
        items: 1
      },

      768: {
        items: 2
      }
    }
  })
})
