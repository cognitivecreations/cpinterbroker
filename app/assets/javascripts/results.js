//= require lodash
//= require js-routes
//= require vue
//= require results/package
//= require results/modal

$(document).ready(function() {
  if (gon.results) {
    // Inquiry.init({
    //   carId: gon.results.car.id,
    //   region: gon.results.region,
    //   hasCloseButton: false
    // })
  }
})

function handleVueDestruction(vue) {
  document.addEventListener('turbolinks:visit', function teardown() {
    vue.$destroy()
    document.removeEventListener('turbolinks:visit', teardown)
  })
}

var TurbolinksAdapter = {
  beforeMount: function() {
    if (this.$el.parentNode) {
      handleVueDestruction(this)
      this.$originalEl = this.$el.outerHTML
    }
  },
  destroyed: function() {
    this.$el.outerHTML = this.$originalEl
  }
}

var Results = {
  init: function() {
    Vue.component('package', VuePackage)
    Vue.component('modal', VueModal)

    Results.app = new Vue({
      el: '#results-panel',
      mixins: [TurbolinksAdapter],
      data: {
        sortOrder: { field: 'final_price', direction: 'asc' },
        filters: !!gon.results.service_id ? [gon.results.service_id] : [1],
        packages: gon.results.packages,
        services: gon.results.services,
        features: gon.results.features,
        selectedPackageIds: [],
        modalVisible: false,
        isMobile: false
      },
      computed: {
        computedPackages: function() {
          var _this = this

          return _this.sortedPackages(
            _.isEmpty(_this.filters)
              ? _this.packages
              : _this.filteredPackages(_this.packages)
          )
        },
        selectedPackages: function() {
          var _this = this

          return _.map(_this.selectedPackageIds, function(packageId) {
            return _.find(_this.packages, { id: packageId })
          })
        },
        comparable: function() {
          return _.inRange(
            this.selectedPackageIds.length,
            2,
            this.isMobile ? 3 : 4
          )
        }
      },
      methods: {
        hasFilter: function(id) {
          return _.includes(this.filters, id)
        },
        toggleFilter: function(id) {
          this.filters = _.xor(this.filters, [id])
        },
        toggleSelected: function(id, value) {
          value
            ? this.selectedPackageIds.push(id)
            : this.selectedPackageIds.splice(
                this.selectedPackageIds.indexOf(id),
                1
              )
        },
        openModal: function() {
          this.modalVisible = true
        },
        closeModal: function() {
          this.modalVisible = false
        },
        sortedPackages: function(packages) {
          var _this = this

          return _.orderBy(
            packages,
            function(pkg) {
              if (_.has(pkg[_this.sortOrder.field], 'fractional')) {
                return pkg[_this.sortOrder.field].fractional
              } else if (_.has(pkg[_this.sortOrder.field], 'name')) {
                return pkg[_this.sortOrder.field].name
              } else if (_.has(pkg[_this.sortOrder.field], 'id')) {
                return pkg[_this.sortOrder.field].id
              } else {
                return pkg[_this.sortOrder.field]
              }
            },
            _this.sortOrder.direction
          )
        },
        filteredPackages: function(packages) {
          var _this = this

          return _.filter(_this.packages, function(pkg) {
            return _this.hasFilter(pkg.service.id)
          })
        },
        setSortOrder: function(field) {
          var _this = this

          if (_this.sortOrder.field === field) {
            _this.sortOrder.direction =
              _this.sortOrder.direction === 'asc' ? 'desc' : 'asc'
          } else {
            _this.sortOrder = { field: field, direction: 'asc' }
          }
        },
        sortOrderClass: function(field) {
          var _this = this

          return [
            'fa fa-fw',
            {
              'fa-sort': field !== _this.sortOrder.field,
              'fa-sort-asc':
                field === _this.sortOrder.field &&
                _this.sortOrder.direction === 'asc',
              'fa-sort-desc':
                field === _this.sortOrder.field &&
                _this.sortOrder.direction === 'desc'
            }
          ]
        }
      },
      mounted: function() {
        var _this = this
        window.addEventListener('load', function() {
          if (this.innerWidth < 480) _this.isMobile = true
        })
      }
    })
  }
}
