$(document).ready(function () {
  $('form[name="summary-inquiry-form"] #submit-button').click(function () {
    $('#inquiry-posted').addClass('hidden')
    $('#inquiry-post-fail').addClass('hidden')
    var email = $('#summary-email').val()
    var phoneNumber = $('#summary-phone-number').val()
    var message = $('#summary-message').val()
    var name = $('#summary-name').val()
    var carId = $('#summary-car-id').val()
    var packageId = $('#summary-package-id').val()
    // var serviceId = $('#summary-service-id').val()
    var region = $('#summary-region').val()

    if (validateInquiryForm(name, email, phoneNumber, message)) {
      $.post('/inquiries', {
        name: name,
        email: email,
        phone_number: phoneNumber,
        car_id: carId,
        package_id: packageId,
        // service_id: serviceId,
        origin: origin,
        region: region,
        message: message
      })
      .done(function (response) {
        $('#inquiry-posted').removeClass('hidden')
        $('form[name="summary-inquiry-form"]').get(0).reset()
        console.log(response)
      })
      .error(function (response) {
        $('#inquiry-post-fail').removeClass('hidden')
        console.error(response)
      })
    }
  })
})

function validateInquiryForm (name, email, phoneNumber, message) {
  $('.validation').addClass('hidden')
  var result = true
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var rpN = /^0[1896]\d{8}$/;
  var rm = /^$|^\s+$/;
  if (rm.test(name)) {
    $('.validation#validation-name').removeClass('hidden')
    result = false
  }
  if (!re.test(email)){
    $('.validation#validation-email').removeClass('hidden')
    result = false
  }
  if (!rpN.test(phoneNumber)) {
    $('.validation#validation-phone-number').removeClass('hidden')
    result = false
  }
  if (rm.test(message)) {
    $('.validation#validation-message').removeClass('hidden')
    result = false
  }
  return result
}
