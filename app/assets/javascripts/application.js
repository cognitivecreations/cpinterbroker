// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require jquery
//= require jquery_ujs
//= require jquery.sticky
//= require bootstrap
//= require owl.carousel.min
//= require inquiry
//= require summary
//= require service_categories
//= require main
//= require_self

window.gon = {}

document.addEventListener('turbolinks:load', function() {
  $(window).on('resize', function() {
    adjustSignInFormLinks()
    adjustBanner()
  })

  enableStickyNavbar()
  adjustBanner()
  enableTooltip()
  adjustSignInFormLinks()
})

function enableStickyNavbar() {
  //Navbar sticky
  $('nav.navbar').sticky({
    mode: 'animate',
    speed: 0.1,
    widthFromWrapper: false
  })
}

function adjustBanner() {
  $('.banner').height($(window).width() / 3)
}

function enableTooltip() {
  // Hide tooltips on xs and sm screens
  if (window.matchMedia('(min-width: 991px)').matches) {
    $('[data-toggle="tooltip"]').tooltip()
  }
}

function adjustSignInFormLinks() {
  var $links = $('#links')

  if (window.matchMedia('(max-width: 768px)').matches) {
    //Remove pull right of sign in form on xs screen size
    $links.removeClass('pull-right')
    $links.addClass('btn-group-justified')
  } else {
    $links.addClass('pull-right')
    $links.removeClass('btn-group-justified')
  }
}
