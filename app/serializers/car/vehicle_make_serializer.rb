class Car::VehicleMakeSerializer < ActiveModel::Serializer
  attributes :id, :name, :is_popular

  def id
    object.vehicle_make.id
  end

  def name
    object.vehicle_make.name
  end

  def is_popular
    object.vehicle_make.is_popular
  end
end
