class Car::VehicleModelSerializer < ActiveModel::Serializer
  attributes :id, :name, :is_popular

  def id
    object.vehicle_model.id
  end

  def name
    object.vehicle_model.name
  end

  def is_popular
    object.vehicle_model.is_popular
  end
end
