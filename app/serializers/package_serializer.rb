class PackageSerializer < ActiveModel::Serializer
  include MoneyRails::ActionViewExtension

  attributes :id, :name, :sum_insured, :excess_payment, :actual_price, :final_price, :repair

  belongs_to :company
  belongs_to :service

  has_many :cars, if: -> { instance_options[:include_cars] }
  has_many :features
  has_many :coverage_handlers do
    object.coverage_handlers.includes(:coverage).order('coverages.priority ASC')
  end

  def sum_insured
    { fractional: object.sum_insured.fractional, display_value: humanized_money_with_symbol(object.sum_insured) }
  end

  def excess_payment
    { fractional: object.excess_payment.fractional, display_value: humanized_money_with_symbol(object.excess_payment) }
  end

  def actual_price
    { fractional: object.actual_price.fractional, display_value: humanized_money_with_symbol(object.actual_price) }
  end

  def final_price
    { fractional: object.final_price.fractional, display_value: humanized_money_with_symbol(object.final_price) }
  end

  def repair
    object.repair || '-'
  end
end
