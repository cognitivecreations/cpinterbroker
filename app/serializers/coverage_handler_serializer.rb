class CoverageHandlerSerializer < ActiveModel::Serializer
  include MoneyRails::ActionViewExtension

  attributes :id, :value, :name, :display_value

  def display_value
    object.display_value.is_a?(Money) ? humanized_money_with_symbol(object.display_value) : object.display_value
  end
end
