class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :logo

  def logo
    object.logo.url(instance_options[:version])
  end
end
