class CarSerializer < ActiveModel::Serializer
  attribute :id
  attribute :trim, key: :name
  attribute :as_string
end
