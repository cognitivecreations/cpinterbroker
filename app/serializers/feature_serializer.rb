class FeatureSerializer < ActiveModel::Serializer
  attributes :id, :name, :icon

  def icon
    object.icon.url(instance_options[:version])
  end
end
