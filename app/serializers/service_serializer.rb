class ServiceSerializer < ActiveModel::Serializer
  attributes :id, :short_name, :name, :image, :icon

  def short_name
    object.name.chomp_left('ประกันภัยรถยนต์')
  end

  def image
    object.image.url(instance_options[:version])
  end

  def icon
    object.icon.url(instance_options[:version])
  end
end
