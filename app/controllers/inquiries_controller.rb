class InquiriesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    current_user ||= User.create_with(password: Devise.friendly_token[0, 10], inquiry_generated: true).
                       find_or_create_by(email: params[:email])

    params[:origin] = request.referrer

    @inquiry = current_user.inquiries.create(inquiry_params)

    if @inquiry.persisted?
      render json: @inquiry
    else
      render json: { message: @inquiry.errors }, status: 400
    end
  end

  private

  def inquiry_params
    params.permit(:email, :phone_number, :name, :region, :package_id, :car_id, :service_id, :origin, :message)
  end
end
