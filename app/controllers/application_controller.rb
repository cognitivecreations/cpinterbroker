class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :navbar_data
  protect_from_forgery with: :exception

  private

  def navbar_data
    unless params[:controller].match(/^admin/)
      @navbar = {
        insurance: ServiceCategory.find_by(identifier: 'insurance'),
        finance: ServiceCategory.find_by(identifier: 'finance'),
        tax: ServiceCategory.find_by(identifier: 'tax')
      }
    end
  end
end
