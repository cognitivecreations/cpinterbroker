class CarsController < ApplicationController
  def trims
    render json: Car.trims_by(params[:year_id], params[:vehicle_make_id], params[:vehicle_model_id])
  end
end
