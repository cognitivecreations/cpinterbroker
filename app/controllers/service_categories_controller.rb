class ServiceCategoriesController < ApplicationController
  before_action :fetch_data, only: [:calculator, :results, :summary]
  before_action :set_title, only: [:calculator, :results, :summary]
  before_action :set_variant, only: [:results]

  def show
    if params[:id] != 'insurance'
      @service_category = ServiceCategory.find(params[:id])
      render :show_table
    else
      @insurance_service_category =  ServiceCategory.includes(:services).find('insurance')
      @insurances = @insurance_service_category.services
      @non_motor_insurance_service_category = ServiceCategory.includes(:services).find('insurance-non-motor')
      @non_motor_insurances = @non_motor_insurance_service_category.services
    end
  end

  def results
    @car = Car.find(params[:calculate][:vehicle_trim])
    @packages = @car.packages.includes(:company, :service, :features, coverage_handlers: :coverage).order(service_id: :asc)

    gon.results = {
      car: @car.as_json,
      packages: @packages.map { |package| PackageSerializer.new(package, version: :small) },
      services: @service_category.services.map { |service| ServiceSerializer.new(service, version: :small, short: true) },
      features: Feature.all.map { |feature| FeatureSerializer.new(feature, version: :small) },
      region: params[:calculate][:region],
      service_id: params[:calculate][:service_id].to_i
    }
  end

  def summary
    @car = Car.find(params[:car_id])
    package = Package.includes(:company, :service, :features, coverage_handlers: :coverage).find(params[:package_id])
    @package = ::DeepStruct.new(PackageSerializer.new(package, version: :small).to_hash)
    @region = params[:region]
  end

  private

  def fetch_data
    @service_category = ServiceCategory.find_by(identifier: 'insurance')
    @services = @service_category.services
    @years = Year.order(name: :desc).all
    @companies = Company.all
  end

  def set_title
    @title = "CP Interbroker - Insurance Calculator"
  end

  def set_variant
    request.variant = :mobile if browser.device.mobile?
  end
end
