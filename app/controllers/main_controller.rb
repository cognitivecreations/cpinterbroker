class MainController < ApplicationController
	def index
		@banner_links = BannerLink.active
		@service_categories = ServiceCategory.first(3)
		@promotions = Promotion.active
	end
end
