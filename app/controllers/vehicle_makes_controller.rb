class VehicleMakesController < ApplicationController
  def index
    render json: Car.vehicle_makes_by(params[:year_id]), each_serializer: Car::VehicleMakeSerializer
  end
end
