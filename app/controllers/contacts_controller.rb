class ContactsController < ApplicationController
  def index
    @contact = ContactUsRecord.new
  end

  def create
    @contact = ContactUsRecord.create(permitted_params)
    ContactMailer.contact_email(@contact).deliver_later
    redirect_to contacts_path, notice: t('mailer.complete')
  end

  private

  def permitted_params
    params.require(:contact_us_record).permit(:name, :email, :phone_number, :subject, :message)
  end
end
