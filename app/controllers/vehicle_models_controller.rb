class VehicleModelsController < ApplicationController
  def index
    render json: Car.vehicle_models_by(params[:year_id], params[:vehicle_make_id]), each_serializer: Car::VehicleModelSerializer
  end

  def active_admin_index
    render json: VehicleModel.by_make(params[:vehicle_make_id])
  end
end
