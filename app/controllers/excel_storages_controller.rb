class ExcelStoragesController < ApplicationController
  def progress
    excel_storage = ExcelStorage.find(params[:id])
    render json: { progress: Sidekiq::Status::pct_complete(excel_storage.job_id) }
  end
end
