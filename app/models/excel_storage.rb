class ExcelStorage < ActiveRecord::Base
  validates_presence_of :file

  mount_uploader :file, ExcelUploader
  after_commit :parse_excel_later, on: [:create]

  # def parse_excel
  #   completed_rows = []
  #   incomplete_rows = []

  #   require 'spreadsheet'
  #   Spreadsheet.client_encoding = 'UTF-8'

  #   path = self.file.path if Rails.env.development?
  #   path = open(self.file.url) if Rails.env.production?
  #   book = Spreadsheet.open(path)
  #   puts book.worksheets
  #   sheet1 = book.worksheet(0)
  #   sheet1.each_with_index do |row, index|
  #     puts "Current Row: #{index}"

  #     next if index < 1
  #     break if row.compact.blank?

  #     result = ExcelRow.new(row)
  #     package_identifier = result.package_identifier

  #     if package_identifier.present? and package = Package.find_by(package_identifier: package_identifier)
  #     else
  #       package = Package.new({
  #         package_identifier: package_identifier,
  #         name: result.package_name,
  #         sum_insured: result.sum_insured,
  #         excess_payment: result.excess_payment,
  #         actual_price: result.actual_price,
  #         discount_type: result.discount_type,
  #         discount_value: result.discount_value,
  #         description: result.description,
  #         repair_site: result.repair_site,
  #         company_id: result.company_id,
  #         service_id: result.service_id,
  #         feature_ids: result.feature_ids,
  #         coverage_handlers: result.coverage_handlers
  #       })
  #     end

  #     if package.save
  #       car_ids = result.car_ids
  #       if car_ids.present?
  #         values = car_ids.map { |car_id| "(#{car_id}, #{package.id})" }.join(',')
  #         sql = "insert into cars_packages(car_id, package_id) values #{values}"
  #         ActiveRecord::Base.connection.execute(sql)
  #       end
  #       completed_rows << index
  #     else
  #       incomplete_rows << index
  #       puts package.errors.inspect
  #     end
  #   end
  #   puts "done"
  #   puts "completed_rows: #{completed_rows}"
  #   puts "incomplete_rows: #{incomplete_rows}"
  # end

private

  def parse_excel_later
    ['packages', 'cars_packages', 'features_packages', 'coverage_handlers'].each do |table_name|
      ActiveRecord::Base.connection.execute(
        "DELETE FROM #{table_name}"
      )
    end

    ['packages'].each do |table_name|
      ActiveRecord::Base.connection.execute(
        "ALTER SEQUENCE #{table_name}_id_seq RESTART WITH 1"
      )
    end
    self.update_columns(job_id: ExcelParserWorker.perform_async(self.id))
  end
end
