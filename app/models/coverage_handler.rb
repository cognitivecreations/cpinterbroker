class CoverageHandler < ActiveRecord::Base
  belongs_to :package
  belongs_to :coverage

  delegate :name, :value_type, to: :coverage

  def to_s
    "#{coverage.name} - #{value.to_s}"
  end

  def display_value
    coverage.value_type == 'amount' ? value.to_money : "#{value.to_i} คน"
  end
end
