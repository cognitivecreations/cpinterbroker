class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  after_create :send_password_setup_email, if: :inquiry_generated?

  has_many :inquiries

  def to_s
    self.email
  end

  def send_password_setup_email
    # UserMailer.password_setup(self).deliver_later
  end

  def generate_reset_password_token
    set_reset_password_token
  end
end
