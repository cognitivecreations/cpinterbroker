class VehicleMake < ActiveRecord::Base
  has_many :vehicle_models
  has_many :cars

  validates :name, presence: true, uniqueness: true
end
