class Feature < ActiveRecord::Base
  has_and_belongs_to_many :packages

  mount_uploader :icon, SquareUploader
end
