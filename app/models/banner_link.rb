class BannerLink < ActiveRecord::Base
	mount_uploader :image, BannerUploader

	scope :active, -> { where(active: true) }

  validates_presence_of :image, :link
end
