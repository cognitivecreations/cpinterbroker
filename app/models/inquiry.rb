class Inquiry < ActiveRecord::Base
  belongs_to :user
  belongs_to :service
  belongs_to :car
  belongs_to :package

  validates :phone_number, length: { minimum: 8, maximum: 10 }

  def email
    self[:email] || self.user&.email
  end
end
