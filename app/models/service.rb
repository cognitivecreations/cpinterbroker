class Service < ActiveRecord::Base
	belongs_to :service_category
  has_and_belongs_to_many :packages

  mount_uploader :image, SquareUploader
  mount_uploader :icon, SquareUploader
end
