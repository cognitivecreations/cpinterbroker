class Year < ActiveRecord::Base
  validates :name,
            presence: true,
            uniqueness: true,
            numericality: {
              only_integer: true,
              greater_than_or_equal_to: 1900,
              less_than_or_equal_to: Date.today.year + 10
            },
            format: {
              with: /(19|20)\d{2}/i,
              message: 'should be a four-digit year'
            }
end
