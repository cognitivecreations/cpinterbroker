class Promotion < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  scope :active, -> { where(active: true) }

  validates_presence_of :image
end
