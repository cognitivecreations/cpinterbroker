class ServiceCategory < ActiveRecord::Base
	extend FriendlyId
  friendly_id :identifier, use: [:slugged, :finders]

	mount_uploader :banner_image, BannerUploader
	mount_uploader :homepage_image, SquareUploader

	has_many :services, dependent: :destroy
end
