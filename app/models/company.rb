class Company < ActiveRecord::Base
  has_many :packages

  mount_uploader :logo, SquareUploader

  validates_presence_of :name#, :logo, :link
  validates_uniqueness_of :name
end
