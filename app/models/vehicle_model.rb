class VehicleModel < ActiveRecord::Base
  belongs_to :vehicle_make
  has_many :cars

  validates :name, presence: true, uniqueness: { scope: :vehicle_make }

  scope :by_make, -> (make_id) { where(vehicle_make_id: make_id) }
end
