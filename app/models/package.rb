class Package < ActiveRecord::Base
  DISCOUNT_TYPES = ['percentage', 'amount'].freeze
  REPAIR_SITES = ['none', 'garage', 'shop', 'both'].freeze
  REPAIR_SITES_TH = [[], ["ซ่อมอู่"], ["ซ่อมห้าง"], ["ซ่อมอู่", "ซ่อมห้าง"]].freeze
  SERVICE_LEVELS = ['1', '2', '3', '2+', '3+'].freeze

  belongs_to :company
  belongs_to :service
  has_and_belongs_to_many :cars
  has_and_belongs_to_many :features
  has_many :coverage_handlers
  has_many :coverages, through: :coverage_handlers

  accepts_nested_attributes_for :coverage_handlers, allow_destroy: true
  accepts_nested_attributes_for :cars
  accepts_nested_attributes_for :features

  validates_presence_of :company, :service

  monetize :sum_insured_satangs
  monetize :excess_payment_satangs
  monetize :actual_price_satangs

  def discount
    (discount_type == 'percentage' ? discount_value.to_s + '%' : ('&#x0E3F;' + discount_value.ceil.to_s).html_safe) unless discount_value.nil?
  end

  def final_price
    self.actual_price - self.actual_discount
  end

  def actual_discount
    if discount_type == 'percentage'
      self.net_price * discount_value / 100
    else
      self.net_price - discount_value.to_money
    end
  end

  def vat
    (self.net_price + self.duty_fee) * 7 / 100
  end

  def net_price
    self.price_before_tax - self.duty_fee
  end

  def duty_fee
    self.price_before_tax - (self.price_before_tax * 100 / 100.4)
  end

  def price_before_tax
    self.actual_price * 100 / 107
  end

  def repair
    REPAIR_SITES_TH[REPAIR_SITES.index(self.repair_site)].join(', ') if self.repair_site
  end

  def self.find_car
    Car.where(year: Year.last).map { |c| [c, c.packages.count] }.max_by { |p| p[1] }[0].to_s
  end
end
