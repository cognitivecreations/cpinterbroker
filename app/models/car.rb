class Car < ActiveRecord::Base
  belongs_to :year
  belongs_to :vehicle_make
  belongs_to :vehicle_model
  has_and_belongs_to_many :packages

  validates_presence_of :year, :vehicle_make, :vehicle_model, :trim

  scope :by_year, -> (year_id) { where(year_id: year_id) }
  scope :by_make, -> (make_id) { where(vehicle_make_id: make_id) }
  scope :by_model, -> (model_id) { where(vehicle_model_id: model_id) }

  def to_s
    "#{year.name} - #{vehicle_make.name} - #{vehicle_model.name} - #{trim}"
  end

  alias_method :as_string, :to_s

  def self.vehicle_makes_by(year_id)
    self.includes(:vehicle_make).
      joins(:vehicle_make).
      select(:vehicle_make_id).
      distinct(:vehicle_make_id).
      by_year(year_id).
      order('vehicle_makes.is_popular DESC, vehicle_makes.name ASC')
  end

  def self.vehicle_models_by(year_id, make_id)
    self.includes(:vehicle_model).
      joins(:vehicle_model).
      select(:vehicle_model_id).
      distinct(:vehicle_model_id).
      by_year(year_id).
      by_make(make_id).
      order('vehicle_models.is_popular DESC, vehicle_models.name ASC')
  end

  def self.trims_by(year_id, make_id, model_id)
    self.by_year(year_id).
      by_make(make_id).
      by_model(model_id)
  end
end
