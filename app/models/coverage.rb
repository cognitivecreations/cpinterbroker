class Coverage < ActiveRecord::Base
  VALUE_TYPES = ["amount", "people"]

  has_many :coverage_handlers
  has_many :packages, through: :coverage_handlers

  validates :name, presence: true, uniqueness: true
  validates :value_type, presence: true
end
