module ActiveAdminHelper

  def beautify(resources)
    container = ""
    resources.each do |resource|
      container += content_tag(:p, resource, style: "width: 200px;")
    end
    content_tag(:div, raw(container))
  end
end
