module ApplicationHelper

  def baht(value)
    ("&#x0E3F;" + value.to_s).html_safe
  end

  def compare_tooltip(title)
    icon('question-circle', title: title, data: {toggle: "tooltip", placement: "right"}, class: "compare-tooltip")
  end

  def t(args)
    translation = super(args)
    translation.include?("span") ? args.to_s : translation
  end
end
