module MailerHelper
  def email_image_tag(image, **options)
    path = Rails.root.join("app/assets/images/#{image}")
    attachments.inline[image] = File.read(path)

    image_tag attachments[image].url, **options
  end
end
