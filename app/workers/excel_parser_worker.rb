class ExcelParserWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker

  sidekiq_options failures: :exhausted, retry: false

  def perform(id)
    excel_storage = ExcelStorage.find(id)
    excel_storage.update_columns(status: 'started')
    # excel_storage.parse_excel

    completed_rows = []
    incomplete_rows = []

    require 'spreadsheet'
    Spreadsheet.client_encoding = 'UTF-8'

    path = excel_storage.file.path if Rails.env.development?
    path = open(excel_storage.file.url) if Rails.env.production?
    book = Spreadsheet.open(path)
    logger.info book.worksheets
    sheet1 = book.worksheet(0)

    total sheet1.last_row_index

    sheet1.each_with_index do |row, index|
      logger.info "Current Row: #{index}"
      at (index + 1)

      next if index < 1
      break if row.compact.blank?

      result = ExcelRow.new(row)
      package_identifier = result.package_identifier

      if package_identifier.present? and package = Package.find_by(package_identifier: package_identifier)
      else
        package = Package.new({
          package_identifier: package_identifier,
          name: result.package_name,
          sum_insured: result.sum_insured,
          excess_payment: result.excess_payment,
          actual_price: result.actual_price,
          discount_type: result.discount_type,
          discount_value: result.discount_value,
          description: result.description,
          repair_site: result.repair_site,
          company_id: result.company_id,
          service_id: result.service_id,
          feature_ids: result.feature_ids,
          coverage_handlers: result.coverage_handlers
        })
      end

      if package.save
        car_ids = result.car_ids
        if car_ids.present?
          values = car_ids.map { |car_id| "(#{car_id}, #{package.id})" }.join(',')
          sql = "insert into cars_packages(car_id, package_id) values #{values}"
          ActiveRecord::Base.connection.execute(sql)
        end
        completed_rows << index
      else
        incomplete_rows << index
        logger.info package.errors.inspect
      end
    end
    logger.info "done"
    logger.info "completed_rows: #{completed_rows}"
    logger.info "incomplete_rows: #{incomplete_rows}"

    excel_storage.update_columns(job_id: nil, status: 'completed', invalid_rows: incomplete_rows)
  rescue => e
    logger.error e.inspect
    logger.error e.backtrace.inspect
    excel_storage.update_columns(job_id: nil, status: 'failed')
  end
end
