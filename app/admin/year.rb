ActiveAdmin.register Year do
  permit_params :name

  menu parent: I18n.t('active_admin.menu.cars')
end
