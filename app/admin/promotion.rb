ActiveAdmin.register Promotion do
  permit_params :link, :image, :remote_image_url, :active

  index do
    selectable_column
    id_column
    column :image do |promotion|
      image_tag promotion.image.url, style: 'height: 50px'
    end
    column :link do |promotion|
      link_to promotion.link, promotion.link
    end
    column :active
    actions
  end

  show do
    attributes_table do
      row :id
      row :image do
        image_tag promotion.image.url, style: 'width: 100%; max-width: 1140px; max-height: 300px'
      end
      row :link
      row :active do
        promotion.active ? t('active_admin.status_tag.yes') : t('active_admin.status_tag.no')
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    if promotion.image?
      panel t('active_admin.current_image'), style: 'text-align: center' do
        image_tag promotion.image.url, style: 'width: 100%; max-width: 1140px; max-height: 300px'
      end
    end
    f.inputs t('active_admin.image') do
      f.input :image, label: t('active_admin.upload_image')
      f.input :remote_image_url, label: t('active_admin.or_input_link')
    end
    f.inputs do
      f.input :link
      f.input :active
    end
    f.actions
  end
end
