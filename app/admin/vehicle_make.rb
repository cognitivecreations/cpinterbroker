ActiveAdmin.register VehicleMake do
  permit_params :name, :is_popular

  menu parent: I18n.t('active_admin.menu.cars')

  show do
    attributes_table do
      row(:id) { resource.id }
      row(:name) { resource.name }
      boolean_row(:is_popular) { resource.is_popular }
    end
  end
end
