ActiveAdmin.register User do
	permit_params :email, :password

	menu parent: I18n.t('active_admin.menu.users')

	form do |f|
		f.semantic_errors *f.object.errors.keys
		f.inputs 'User' do
			f.input :email
			f.input :password
		end
		f.actions
	end

	index do
		selectable_column
		id_column
		column :email
		column :current_sign_in_at
		column :last_sign_in_at
		column :created_at
		column :updated_at
		actions
	end
end
