include ActiveAdminHelper

ActiveAdmin.register Package do
  permit_params :company_id, :name, :service_id, :repair_site, :description, :sum_insured, :excess_payment, :actual_price, :discount_type, :discount_value, coverage_handlers_attributes: [:id, :coverage_id, :sort_order, :value, '_destroy'], car_ids: [], feature_ids: []

  menu parent: I18n.t('active_admin.menu.insurance_calculator')

  index do
    selectable_column
    id_column
    column(:name) { |package| link_to(package.name, admin_package_path(package)) }
    column :company
    column :service
    column :cars do |package|
      beautify(package.cars.first(2).collect{ |c| link_to c.to_s, admin_car_path(c) })
    end
    column :repair
    money_column :sum_insured
    money_column :excess_payment
    money_column :actual_price
    column :discount
    column :features do |package|
      beautify(package.features.collect{ |feature| feature.name })
    end
    column :coverage_handlers do |package|
      beautify(package.coverage_handlers.collect{ |coverage_handler| coverage_handler.to_s })
    end
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    tabs do
      tab 'Details' do
        f.inputs do
          f.input :company
          f.input :name
          f.input :service, as: :select, collection: Service.where(service_category: ServiceCategory.find_by(identifier: 'insurance')).collect{ |service| [service.name, service.id] }
          f.input :repair_site, as: :radio, collection: Package::REPAIR_SITES.each_with_index.map { |site, index| [Package::REPAIR_SITES_TH[index].join(', '), site] }
          f.input :description
          f.input :sum_insured
          f.input :excess_payment
          f.input :actual_price
          f.input :discount_type, as: :select, collection: Package::DISCOUNT_TYPES
          f.input :discount_value
        end

        f.inputs 'Features' do
          f.input :features, as: :check_boxes
        end

        f.inputs 'Coverages' do
          f.has_many :coverage_handlers, heading: false, allow_destroy: true, sortable: :sort_order do |ch|
            ch.input :coverage, as: :select, collection: Coverage.all.collect{ |c| [c.name + " - " + c.value_type, c.id] }
            ch.input :value
          end
        end
      end

      tab 'Cars' do
        f.inputs do
          f.input :cars, as: :select, collection: f.object.cars.pluck(:trim, :id), input_html: { class: 'no-selectize' }
        end
      end
    end

    f.actions
  end
end
