ActiveAdmin.register Car do
  permit_params :vehicle_make_id, :vehicle_model_id, :year_id, :trim, :description

  menu parent: I18n.t('active_admin.menu.cars')

  index do
    selectable_column
    id_column
    column :year
    column :trim
    column :vehicle_make
    column :vehicle_model
    column :vehicle_type
    actions
  end

  show do
    attributes_table do
      row :trim
      row :description
      row :year
      row :vehicle_make
      row :vehicle_model
      row :vehicle_type
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :trim
      f.input :vehicle_type
      f.input :description
    end

    f.inputs "Select or create new Year" do
      f.input :year
    end

    f.inputs "Vehicle Make and Vehicle Model" do
      f.input :vehicle_make, as: :select, collection: VehicleMake.all, input_html: { class: 'no-selectize' }
      f.input :vehicle_model, as: :select, input_html: { 'data-option-dependent': true, 'data-option-observed' => 'car_vehicle_make_id', 'data-option-url' => '/vehicle_models_active_admin?vehicle_make_id=:car_vehicle_make_id', class: 'no-selectize' }, collection: (resource.vehicle_make_id ? VehicleMake.find(resource.vehicle_make_id).vehicle_models.collect {|m| [m.name, m.id]} : [])
    end

    f.actions
  end
end
