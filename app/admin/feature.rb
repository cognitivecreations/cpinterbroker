ActiveAdmin.register Feature do
  permit_params :name, :icon

  menu parent: I18n.t('active_admin.menu.insurance_calculator')

  index do
    selectable_column
    id_column
    column :icon do |feature|
      image_tag feature.icon.url(:small), style: "height: 50px"
    end
    column :name
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :icon do |feature|
        image_tag feature.icon.url, style: "height: 100px"
      end
      row :name
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      if f.object.new_record?
        f.input :icon
      else
        li do
          label :icon
          img src: f.object.icon.url, height: '150px'
        end
        f.input :icon, label: "&nbsp;"
      end
      f.input :name
    end
    f.actions
  end
end
