ActiveAdmin.register Coverage do
  menu parent: I18n.t('active_admin.menu.insurance_calculator')

  permit_params :name, :value_type, :priority

  config.sort_order = 'priority_asc'

  index do
    selectable_column
    id_column

    column :name
    column :value_type
    column :priority
    column :created_at
    column :updated_at

    actions
  end

  form do |f|
    f.semantic_errors

    f.inputs do
      f.input :name
      f.input :value_type, as: :select, collection: Coverage::VALUE_TYPES
      f.input :priority
    end

    f.actions
  end
end
