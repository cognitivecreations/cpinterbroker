ActiveAdmin.register ServiceCategory do
  permit_params :name, :slug, :banner_image, :homepage_image, :description

  menu parent: I18n.t('active_admin.menu.services')

  index do
    selectable_column
    id_column
    column :name
    column :banner_image do |service_category|
      image_tag service_category.banner_image.url, height: '50px'
    end
    column :homepage_image do |service_category|
      image_tag service_category.homepage_image.url, height: '50px'
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :slug
      row :identifier
      row :banner_image do |service_category|
        image_tag service_category.banner_image.url, height: "100px"
      end
      row :homepage_image do |service_category|
        image_tag service_category.homepage_image.url, height: "100px"
      end
      row :description do |service_category|
        service_category.description&.html_safe
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name

      if f.object.new_record?
        f.input :banner_image
      else
        li do
          label :banner_image
          img src: f.object.banner_image.url(:mini)
        end
        f.input :banner_image, label: "&nbsp;"
      end

      if f.object.new_record?
        f.input :homepage_image
      else
        li do
          label :homepage_image
          img src: f.object.homepage_image.url(:small)
        end
        f.input :homepage_image, label: "&nbsp;"
      end

      f.input :description, as: :ckeditor
    end
    f.actions
  end
end
