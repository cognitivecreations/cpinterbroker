ActiveAdmin.register VehicleModel do
  permit_params :name, :vehicle_make_id, :is_popular

  menu parent: I18n.t('active_admin.menu.cars')

  index do
    selectable_column
    id_column
    column :vehicle_make
    column(:name) { |vm| link_to vm.name, admin_vehicle_model_path(vm) }
    column :is_popular
    actions
  end

  show do
    attributes_table do
      row(:name) { resource.name }
      row(:vehicle_make) { resource.vehicle_make }
      boolean_row(:is_popular) { resource.is_popular }
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :vehicle_make
      f.input :name
      f.input :is_popular
    end
    f.actions
  end
end
