ActiveAdmin.register ExcelStorage do
  permit_params :file

  menu parent: I18n.t('active_admin.menu.insurance_calculator')

  index do
    selectable_column
    column :id
    column :file
    column :status
    column :progress do |excel_storage|
      content_tag :span, excel_storage.id, style: 'display: none' if excel_storage.job_id
    end
    column :invalid_rows
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :file
    end

    f.actions
  end
end
