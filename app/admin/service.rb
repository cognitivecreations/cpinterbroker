ActiveAdmin.register Service do
  permit_params :name, :service_category_id, :description, :image, :icon, :link

  menu parent: I18n.t('active_admin.menu.services')

  index do
    selectable_column
    column :id
    column :name
    column :image do |service|
      image_tag service.image.url, style: 'height: 50px'
    end
    column :icon do |service|
      image_tag service.icon.url(:small), style: 'height: 50px'
    end
    column :service_category
    column :link
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :description
      row :image do |service|
        image_tag service.image.url, style: 'height: 100px'
      end
      row :service_category_id
      row :created_at
      row :icon do |service|
        image_tag service.icon.url, style: 'height: 100px'
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :service_category
      f.input :name
      f.input :link
      if f.object.new_record?
        f.input :image
      else
        li do
          label :image
          img src: f.object.image.url, height: '150px'
        end
        f.input :image, label: "&nbsp;"
      end
      if f.object.new_record?
        f.input :icon
      else
        li do
          label :icon
          img src: f.object.icon.url, height: '150px'
        end
        f.input :icon, label: "&nbsp;"
      end
      f.input :description
    end
    f.actions
  end
end
