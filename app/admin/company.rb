ActiveAdmin.register Company do
  permit_params :name, :description, :logo, :link, :remote_logo_url

  menu parent: I18n.t('active_admin.menu.insurance_calculator')

  index do
    selectable_column
    id_column
    column :logo do |c|
      image_tag c.logo.url, height: '70', style: "max-width: 70px"
    end
    column :name do |c|
      content_tag(:p, c.name, style: 'width: 300px;')
    end
    column :link
    column :description do |c|
      content_tag(:p, c.description, style: 'width: 300px; height: 120px; overflow: hidden;')
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :logo do
        image_tag company.logo.url, height: '70', style: "max-width: 70px"
      end
      row :name
      row :link
      row :description
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.semantic_errors
    if company.logo?
      panel t('active_admin.current_image'), style: 'text-align: left' do
        image_tag company.logo.url, height: '70', style: "max-width: 70px"
      end
    end
    f.inputs t('active_admin.image') do
      f.input :logo, label: t('active_admin.upload_image')
      f.input :remote_logo_url, label: t('active_admin.or_input_link')
    end
    f.inputs do
      f.input :name
      f.input :link
      f.input :description
    end
    f.actions
  end
end
