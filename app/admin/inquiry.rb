ActiveAdmin.register Inquiry do
  actions :index, :show, :destroy

  index do
    id_column

    column :email
    column :name
    column :phone_number
    column :region
    column :updated_at
    column :created_at

    actions
  end
end
